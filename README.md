To install:
* Clone package to $basePath/packages/glu/b24.app.background
* Add package to "repositories" section of your basic composer.json file (create this section if need):
```json
"repositories": {
    "b24.app.background": {
        "type": "path",
        "url": "packages/glu/b24.app.background",
        "options": {
            "symlink": true
        }
    }
}
```
* Add pair:
```json
"glu/b24.app.background": "@dev"
```
...to your "require" section of basic composer.json

* Run in console:
```shell
composer update glu/b24.app.background
```
(if you want to include/update this package only)

OR:
```shell
composer update
```
(for initial project installation)

* Run installer:
```shell
php artisan b24.app.background:install
```

* After the package is installed, run your application migrations (package registers its migrations path):
```
php artisan migrate
```
