<?php

namespace Glu\B24AppBackground\Console\Commands\Traits;

trait ConsumerConnection
{
    protected function setConsumerOptions(int $consumerConnectionId)
    {
        \app()['consumer_connection_id'] = $consumerConnectionId;

        if (empty($this->option('database'))) {
            $this->input->setOption('database', \env('DB_CONSUMER_CONNECTION_NAME', 'consumer_connection'));
        }

        if (empty($this->option('path'))) {
            $this->input->setOption('path', $this->getDefaultConsumerPaths());
        }
    }

    protected function getDefaultConsumerPaths(): array
    {
        $paths = [];
        $sourceMigrationDirs = [
            'vendor/glu/b24.app.background/src/Db/consumer',
            'database/migrations_consumer',
        ];
        foreach ($sourceMigrationDirs as $dir) {
            if (\is_dir(\base_path().'/'.$dir)) {
                $paths[] = $dir;
            }
        }

        return $paths;
    }
}
