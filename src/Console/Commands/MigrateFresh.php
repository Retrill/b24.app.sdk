<?php

namespace Glu\B24AppBackground\Console\Commands;

use Glu\B24AppBackground\Console\Commands\Traits\ConsumerConnection;
use Illuminate\Database\Console\Migrations\FreshCommand;
use Symfony\Component\Console\Input\InputOption;

class MigrateFresh extends FreshCommand
{
    use ConsumerConnection;

    /**
     * {@inheritdoc}
     */
    protected function getOptions()
    {
        $options = parent::{__FUNCTION__}();
        $options[] = [
            'consumer-connection-id',
            null,
            InputOption::VALUE_OPTIONAL,
            'Consumer connection to use',
        ];

        return $options;
    }

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        if (!empty($this->option('consumer-connection-id'))) {
            $this->setConsumerOptions((int) $this->option('consumer-connection-id'));
        }
        // \app()['consumer_connection_id'] = $this->option('consumer-connection-id');
        parent::handle();
    }
}
