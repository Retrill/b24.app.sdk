<?php

namespace Glu\B24AppBackground\Console\Commands;

use App\Http\Middleware\VerifyCsrfToken;
use Glu\B24AppBackground\Http\VerifyOrigin;
use Glu\LaravelExtensions\Io;
use Illuminate\Console\Command;

class Install extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'b24.app.background:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Installs the package b24.app.background';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->task('Patch phpunit.xml', [$this, 'patchPhpUnit'], '');
        $this->task('Patch VerifyCsrfToken middlware class', [$this, 'patchCsrfClass'], '');
        // $this->task('Perform migrations', [$this, 'migrate'], '');
        $this->task('Publish assets (symlink)', [$this, 'publishAssets'], '');

        return 0;
    }

    protected function patchPhpUnit()
    {
        $targetPath = base_path().'/phpunit.xml';
        if (!file_exists($targetPath)) {
            $this->error("File $targetPath does not exist!");

            return false;
        }
        $xmlView = \simplexml_load_file($targetPath);
        $testsuites = $xmlView->xpath('/phpunit/testsuites/testsuite[@name="glu/b24.app.background"]');
        if (count($testsuites) > 1) {
            $this->error('Duplications found!');

            return false;
        } elseif (1 == count($testsuites)) {
            $this->info('nothing to do');

            return true;
        } else {
            $container = current($xmlView->xpath('/phpunit/testsuites'));
            $newTestsuite = $container->addChild('testsuite', '');
            $newTestsuite->addAttribute('name', 'glu/b24.app.background');
            $directory = $newTestsuite->addChild('directory', './vendor/glu/b24.app.background/tests');
            $directory->addAttribute('suffix', 'Test.php');

            $xmlText = \preg_replace('/<server[^>]*?name=.?APP_ENV.*?>/i', '<!-- $0 -->', $xmlView->asXML());

            return Io::gracefulRewrite($targetPath, $xmlText, 'phpunit');
        }

        return true;
    }

    protected function patchCsrfClass()
    {
        $reflection = new \ReflectionClass(VerifyCsrfToken::class);
        if (in_array(VerifyOrigin::class, $reflection->getTraitNames())) {
            $this->info('nothing to do');

            return true;
        }

        $sourceFilepath = $reflection->getFileName();

        $patchedCode = preg_replace(
            '/class[\s\S]*?\{/', '$0'."\n    use \\".VerifyOrigin::class.";\n",
            file_get_contents($reflection->getFileName())
        );

        return Io::gracefulRewrite($sourceFilepath, $patchedCode, 'csrf');
    }

    protected function publishAssets()
    {
        $targetPath = \public_path('glu/b24.app.background');
        if (file_exists($targetPath)) {
            if (is_link($targetPath)) {
                $this->info('Public path "'.$targetPath.'" already exists, nothing to do');

                return true;
            } else {
                $this->error("File $targetPath exists, but isn't a link!");

                return false;
            }
        }
        $sourcePath = \dirname(__DIR__, 2).'/public';
        if (!Io::mkdir(\dirname($targetPath))) {
            $this->error('Cannot create path '.$targetPath);
        }
        // $this->info("source: $sourcePath");
        // $this->info("target: $targetPath");

        return \symlink($sourcePath, $targetPath);
    }

    // migrations moved to service provider
    /* protected function migrate()
    {
        $appMigrDir = \dirname(__DIR__, 2).'/Db/migrations';
        $cmd = 'migrate --path='.$appMigrDir;
        $this->info($cmd);
        \Artisan::call($cmd);
    } */
}
