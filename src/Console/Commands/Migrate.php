<?php

namespace Glu\B24AppBackground\Console\Commands;

use Glu\B24AppBackground\Console\Commands\Traits\ConsumerConnection;
use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Database\Console\Migrations\MigrateCommand as BaseMigrateCommand;
use Illuminate\Database\Migrations\Migrator;

class Migrate extends BaseMigrateCommand
{
    use ConsumerConnection;

    public function __construct(Migrator $migrator, Dispatcher $dispatcher)
    {
        // exit($this->signature);
        $this->signature .= '
                {--consumer-connection-id= : Run migrations in a specific consumer connection.}
        ';

        parent::__construct($migrator, $dispatcher);
    }

    /**
     * {@inheritdoc}
     */
    public function handle()
    {
        if (!empty($this->option('consumer-connection-id'))) {
            $this->setConsumerOptions((int) $this->option('consumer-connection-id'));
        }
        parent::handle();
    }
}
