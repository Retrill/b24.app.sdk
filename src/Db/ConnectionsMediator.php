<?php

namespace Glu\B24AppBackground\Db;

use Glu\B24AppBackground\Models\ConsumerConnection;
use Illuminate\Support\Str;

class ConnectionsMediator extends \Illuminate\Config\Repository
{
    protected $has = '';

    public function get($key, $default = null)
    {
        dump(__METHOD__);
        $parentValue = parent::{__FUNCTION__}($key, $default);
        if (!is_null($parentValue)) {
            return $parentValue;
        }

        $this->getConsumerConnections();

        return parent::{__FUNCTION__}($key, $default);
    }

    public function getMany($keys)
    {
        throw new \Exception('Unexpected use case!');

        return parent::{__FUNCTION__}($keys);
    }

    public function all()
    {
        dump('all called!');

        return $this->items;
    }

    public function has($key)
    {
        if ($this->has == $key) {
            return false;
        }
        dump(__METHOD__.' > '.$key);
        if ($parentValue = parent::{__FUNCTION__}($key)) {
            return $parentValue;
        }
        $this->has = $key;
        $this->getConsumerConnections();
        dump('items: ', $this->items);
        $this->has = '';

        $result = parent::{__FUNCTION__}($key);
        dump("parent of $key: ", $result);

        return $result;
    }

    protected function getConsumerConnections(): void
    {
        dump(__METHOD__);
        $consumerConnections = ConsumerConnection::all();
        dump('connections:', $consumerConnections);
        foreach ($consumerConnections as $connection) {
            $connectionAr = $connection->toArray();
            if ('mysql' == $connection->driver) {
                $connection['database'] = Str::substitute($connectionAr, env('DB_CONSUMER_DATABASE'));
            }
            $this->set(Str::substitute($connectionAr, env('DB_CONSUMER_CONNECTION')), $connectionAr);
        }
    }
}
