<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ConsumerCreateTokensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tokens', function (Blueprint $table) {
            $table->id();
            $table->unsignedInteger('bx_user_id')->nullable(false)->default(0);
            $table->timestamp('created_at')->useCurrent()->nullable(false);
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate()->nullable(false);
            // Вроде поле ниже не нужно, т.к. обновляется строка только в случае получения нового токена,
            // это будет отражено в updated_at. И отдельного поля с датой последнего получения не нужно.
            // $table->timestamp('checked_out_at')->useCurrent()->nullable(false);
            $table->timestamp('valid_through')->useCurrent()->nullable(false);
            $table->string('auth_token', 255)->nullable(false)->unique();
            // $table->smallInteger('lifespan')->nullable(false);
            $table->string('refresh_token', 255)->nullable(false);
            $table->char('lang', 2)->nullable(false)->default('ru');
            $table->boolean('system')->nullable(false)->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tokens');
    }
}
