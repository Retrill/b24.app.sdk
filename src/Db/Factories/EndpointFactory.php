<?php

namespace Glu\B24AppBackground\Db\Factories;

use Glu\B24AppBackground\Models\Endpoint;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class EndpointFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Endpoint::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'fingerprint' => Str::randomDict(255, 'A-Z a-z \d'),
            'uri' => '/rest/',
            'app_id' => '',
            'app_secret' => '',
        ];
    }

    public function make($attributes = [], ?Model $parent = null)
    {
        return parent::{__FUNCTION__}(static::canonicalizeAttrs($attributes), $parent);
    }

    public function create($attributes = [], ?Model $parent = null)
    {
        return parent::{__FUNCTION__}(static::canonicalizeAttrs($attributes), $parent);
    }

    public static function canonicalizeAttrs(array $attributes): array
    {
        if (isset($attributes['host'])) {
            $attributes['host'] = \mb_strtolower(\rtrim(
                \preg_replace('/^https?:\/\//i', '', $attributes['host']),
                '/'
            ), 'UTF-8');
        }

        return $attributes;
    }
}
