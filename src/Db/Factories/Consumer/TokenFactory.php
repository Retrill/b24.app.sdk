<?php

namespace Glu\B24AppBackground\Db\Factories\Consumer;

use Glu\B24AppBackground\Models\Consumer\Token;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;

class TokenFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Token::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [];
    }

    public function make($attributes = [], ?Model $parent = null)
    {
        return parent::{__FUNCTION__}(static::canonicalizeAttrs($attributes), $parent);
    }

    public function create($attributes = [], ?Model $parent = null)
    {
        return parent::{__FUNCTION__}(static::canonicalizeAttrs($attributes), $parent);
    }

    public static function canonicalizeAttrs(array $attributes): array
    {
        $replacements = [
            'AUTH_ID' => 'auth_token',
            'AUTH_EXPIRES' => 'valid_through',
            'REFRESH_ID' => 'refresh_token',
            'LANG' => 'lang',
        ];
        foreach ($replacements as $from => $to) {
            $attributes[$to] = $attributes[$to] ?? $attributes[$from] ?? null;
        }
        if (\is_numeric($attributes['valid_through']) && $attributes['valid_through'] < 10000) {
            $attributes['valid_through'] = (new \DateTime())->add(new \DateInterval('PT'.$attributes['valid_through'].'S'))->format('Y-m-d H:i:s');
        }
        // $attributes['DOMAIN'] = \preg_replace('/^https?:\/\//i', $attributes['DOMAIN']);

        $attributes = \array_diff_key($attributes, \array_flip(['AUTH_ID', 'AUTH_EXPIRES', 'REFRESH_ID', 'member_id', 'status', 'PLACEMENT', 'PLACEMENT_OPTIONS', 'DOMAIN', 'PROTOCOL', 'LANG', 'APP_SID', 'app_id', 'app_secret']));
        $attributes = \array_filter($attributes);

        // \dump($attributes);
        return $attributes;
    }
}
