<?php

namespace Glu\B24AppBackground\Db\Factories;

use Glu\B24AppBackground\Models\ConsumerConnection;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class ConsumerConnectionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ConsumerConnection::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'host' => 'localhost',
            'username' => Str::randomDict(12, 'a-z \d', 'a-z'),
            'password' => Str::randomDict(20, 'A-Z a-z \d %_', 'A-Z a-z \d'),
        ];
    }

    public function mysql()
    {
        return $this->state(function (array $attributes) {
            return [
                'driver' => 'mysql',
                'username' => '',
                'host' => \env('DB_CONSUMER_HOST', '127.0.0.1'),
                'port' => \env('DB_CONSUMER_PORT', 3306),
                'database' => '',
            ];
        });
    }

    public function sqlite()
    {
        return $this->state(function (array $attributes) {
            return [
                'driver' => 'sqlite',
                'port' => 0,
                'database' => ':memory:',
            ];
        });
    }

    public function configure()
    {
        return $this->afterMaking(function (ConsumerConnection $connection) {
        })->afterCreating(function (ConsumerConnection $connection) {
            $endpointId = $connection->endpoint->id;
            $connection->username = \strtr(\env('DB_CONSUMER_USER_TPL'), [
                '{endpoint_id}' => $endpointId,
                '{connection_id}' => $connection->id,
            ]);
            if ('sqlite' != $connection->driver) {
                $connection->database = \strtr(\env('DB_CONSUMER_DB_TPL'), [
                    '{endpoint_id}' => $endpointId,
                    '{connection_id}' => $connection->id,
                ]);
            }
            $connection->save();

            $driver = \config('database.connections.'.\env('DB_CONNECTION'))['driver'];
            $this->{$driver.'Afteractions'}($connection);
        });
    }

    protected function mysqlAfteractions(ConsumerConnection $connection)
    {
        $queries = [
            'CREATE USER `{username}`@`{host}` IDENTIFIED BY \'{pass}\'',
            'GRANT SELECT, INSERT, UPDATE, DELETE
                ON `{db_grant_tpl}`.* TO `{username}`@`{host}`',
            // 'GRANT CREATE ON TABLE `{db_grant_tpl}`.* TO `{username}`@`{host}`',
            'GRANT ALL PRIVILEGES ON `{db_grant_tpl}`.* TO `{username}`@`{host}`',
            'CREATE DATABASE IF NOT EXISTS `{database}` CHARACTER SET utf8 COLLATE utf8_unicode_ci',
            'FLUSH PRIVILEGES',
        ];

        $replacements = [
            'username' => $connection->username,
            'pass' => $connection->password,
            'host' => \env('DB_CONSUMER_ALLOWED_HOST_FROM', '127.0.0.1'),
            'database' => $connection->database,
            'db_grant_tpl' => \strtr(\env('DB_CONSUMER_DB_GRANT_TPL'), [
                '{endpoint_id}' => $connection->endpoint->id,
                '{connection_id}' => $connection->id,
            ]),
        ];

        foreach ($queries as $query) {
            $query = Str::substitute($replacements, $query);
            DB::statement($query);
        }
        $this->migrateConsumerConnection($connection);
    }

    protected function sqliteAfteractions(ConsumerConnection $connection)
    {
        $this->migrateConsumerConnection($connection);
    }

    protected function migrateConsumerConnection(ConsumerConnection $connection){
        // To do migrations under the consumer connection - we should firstly define connection config at service provider
        $connectionName = \env('DB_CONSUMER_CONNECTION_NAME', 'consumer_connection');
        $sourceMigrationDirs = [
            'vendor/glu/b24.app.background/src/Db/consumer',
            'database/migrations_consumer'
        ];
        foreach ($sourceMigrationDirs as $dir) {
            if (\is_dir(\base_path().'/'.$dir)) {
                \Artisan::call(\sprintf('migrate --database=%s --consumer-connection-id=%u --path=%s', $connectionName, $connection->id, $dir));
            }
        }
    }

    public static function getDefaultMysqlConfig(){
        return [
            'driver' => 'mysql',
            'host' => '',
            'port' => env('DB_PORT', '3306'),
            'database' => '',
            'username' => '',

            'url' => \env('DATABASE_URL'),
            'password' => \env('DB_PASSWORD', ''),
            'unix_socket' => \env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => \extension_loaded('pdo_mysql') ? array_filter([
                \PDO::MYSQL_ATTR_SSL_CA => \env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ];
    }
}
