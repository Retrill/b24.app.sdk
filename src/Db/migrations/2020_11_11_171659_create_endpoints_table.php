<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEndpointsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('endpoints', function (Blueprint $table) {
            $table->id();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->string('host', 512)->nullable(false)->unique();
            // "/rest/" or specific webhook uri:
            $table->string('uri', 512)->nullable(false);
            // bitrix24 member id (may be null if webhook)
            $table->string('code', 64)->nullable(true)->unique();
            // in the case of webhook this value should be false:
            $table->boolean('authenticable')->nullable(false)->default(true);

            $table->string('app_id', 512)->nullable(false);
            $table->string('app_secret', 512)->nullable(false);
            // in the case of webhook this value may be null:
            $table->char('fingerprint', 255)->nullable(true)->unique();
            // $table->unsignedInteger('connection_id')->nullable(false);
            // $table->timestamps()->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('endpoints');
    }
}
