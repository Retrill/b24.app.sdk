<?php

namespace Glu\B24AppBackground\Http;

trait VerifyOrigin
{
    /**
     * The URIs that should be forced to be checked by CSRF verification.
     *
     * @var array
     */
    // protected $forcedCheck = [];

    protected function inExceptArray($request)
    {
        if (!empty($this->forcedCheck)) {
            foreach ($this->forcedCheck as $pattern) {
                if ($request->is($pattern)) {
                    return false;
                }
            }
        }

        // exit($request->header('X-CSRF-TOKEN'));
        // var_dump($request->session()->get('foo'));
        // dd($request->cookie('testn'));
        // echo 'cookie:';
        // var_dump( $_COOKIE);
        // var_dump( $request->cookie('testn'));
        if (parent::{__FUNCTION__}($request)) {
            return true;
        }

        $requestDomain = $request->input('DOMAIN');
        if (empty($requestDomain)) {
            return false;
        }

        $regex = '#https?:\/\/(?<origin>[\w\d\.\-]+)\/*#i';
        if (!is_null($request->server('HTTP_ORIGIN'))) {
            if (!(bool) \preg_match($regex, $request->server('HTTP_ORIGIN'), $matches)) {
                return false;
            }

            return $requestDomain == $matches['origin'];
        }

        // check by http-referer allow only for mozilla users (no origin header)
        if (!(bool) \preg_match('/Mozilla.*Firefox/', $request->server('HTTP_USER_AGENT'))) {
            return false;
        }
        if (!is_null($request->server('HTTP_REFERER'))) {
            if (!(bool) \preg_match($regex, $request->server('HTTP_REFERER'), $matches)) {
                return false;
            }

            return $requestDomain == $matches['origin'];
        }

        return false;
    }
}
