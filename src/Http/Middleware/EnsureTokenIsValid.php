<?php

namespace Glu\B24AppBackground\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;
use Glu\B24RestSdk\Client\Consumer as ConsumerRestClient;

class EnsureTokenIsValid extends Middleware
{
    public function handle(Request $request, \Closure $next)
    {
        $client = \app(ConsumerRestClient::class);
        $isAuthorized = $client->isAuthorized();
        if(!$isAuthorized /*&& $request->expectsJson()*/){
            return response()->json(['error' => 'Not authorized (token invalid?).'], 403);
        }

        return $next($request);
    }
}
