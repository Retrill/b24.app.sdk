<?php

namespace Glu\B24AppBackground;

use Glu\B24AppBackground\Models\Endpoint;
use Glu\B24AppBackground\Models\ConsumerConnection;
use Glu\B24AppBackground\Db\Factories\ConsumerConnectionFactory;
use Glu\B24RestSdk\Credentials\ArrayBased as Credentials;
use Glu\B24RestSdk\Client\Consumer as ConsumerRestClient;
use Illuminate\Database\Connectors\ConnectionFactory;
use Illuminate\Support\ServiceProvider;
use Glu\B24AppBackground\Http\Middleware\EnsureTokenIsValid;
// use Illuminate\Http\Request;

class MainServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->scoped(ConsumerRestClient::class, function ($app) {
            $client = new ConsumerRestClient(Credentials::makeInstance(\request()->all()));

            return $client;
        });
        $this->app->router->aliasMiddleware('checkToken', EnsureTokenIsValid::class);
    }

    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/routes.php');
        $this->loadViewsFrom(__DIR__.'/resources/views', 'glu/b24.app.background');
        if ($this->app->runningInConsole()) {
            $this->commands([
                Console\Commands\Install::class,
            ]);
        }
        $this->loadMigrationsFrom(__DIR__.'/Db/migrations');
        $this->initConsumerConnection();
    }

    protected function initConsumerConnection(): void
    {
        $this->app['db']->extend('consumer_connection', function ($config, $name) {
            // prepare consumer connection config

            $connectionFactory = new ConnectionFactory($this->app);
            $masterConnName = \env('DB_CONNECTION');
            $dbDriver = \config('database.connections.'.\env('DB_CONNECTION'))['driver'];
            // \dd($dbDriver, 'db driver');
            if ('sqlite' == $dbDriver) {
                $consumerConnection = \collect([
                    'driver' => 'sqlite',
                    'url' => \env('DATABASE_URL'),
                    'database' => ':memory:',
                    'prefix' => '',
                    'foreign_key_constraints' => \env('DB_FOREIGN_KEYS', true),
                ]);
            } elseif (empty($this->app['consumer_connection_id'])) {
                // Переменная consumer_connection_id хранит значение одноимённого консольного параметра
                // "consumer_connection_id" при запуске из командной строки artisan (иначе - пустое значение)
                $request = $this->app['request'];
                $endpointCode = $request->input('member_id');
                if (\is_null($endpointCode)) {
                    throw new \Exception('Обращение к соединению, когда клиент не установлен!');
                }

                // we shouldn't use any model here without direct point to connection
                // because of Artisan call on consumer connection database (to initially migrate it)

                // if (!$endpoint = Endpoint::where('code', $request->input('member_id'))->first()) {
                if (!$endpoint = (new Endpoint())->on($masterConnName)->where('code', $request->input('member_id'))->first()) {
                    throw new \Exception('Обращение к соединению незарегистрированного клиента!');
                }
                // if (!$consumerConnection = $endpoint->connections->first()) {
                if (!$consumerConnection = (new ConsumerConnection())->on($masterConnName)->where('endpoint_id', $endpoint->id)->first()) {
                    throw new \Exception('Клиентское соединение не найдено!');
                }
            } elseif (!$consumerConnection = (new ConsumerConnection())->on($masterConnName)->find($this->app['consumer_connection_id'])) {
                throw new \Exception('Клиентское соединение с указанным id не найдено!');
            }
            $consumerConnection = $consumerConnection->toArray();

            $defaultMysqlConfig = ConsumerConnectionFactory::getDefaultMysqlConfig();
            $config = ('sqlite' == $dbDriver) ? $consumerConnection : (\array_intersect_key($consumerConnection, $defaultMysqlConfig) + $config + $defaultMysqlConfig);
            $result = $connectionFactory->make($config, $name);

            return $result;
        });

        /* $this->app->extend('config', function ($config, $app) {
            $config['database.connections'] = new ConnectionsMediator($config['database.connections']);

            return $config;
        }); */
        // \Artisan::call('config:clear');
    }
}
