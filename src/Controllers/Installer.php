<?php

namespace Glu\B24AppBackground\Controllers;

use Glu\B24AppBackground\Models\Consumer\Token;
use Glu\B24AppBackground\Models\ConsumerConnection;
use Glu\B24AppBackground\Models\Endpoint;
use Glu\B24RestSdk\Client;
use Glu\B24RestSdk\Credentials\ModelBased as Credentials;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Installer extends BaseController
{
    use AuthorizesRequests;
    use DispatchesJobs;
    use ValidatesRequests;

    public function __invoke(Request $request, int $step)
    {
        if (empty($request->input('member_id'))) {
            return \response('Incomplete request data (empty required fields)', 406)
                  ->header('Content-Type', 'text/plain');
        }

        $endpoint = $this->initEndpoint($request);

        if (!$consumerConnection = $endpoint->connections->first()) {
            $dbDriver = \config('database.connections.'.\env('DB_CONNECTION'))['driver'];
            $consumerConnection = ConsumerConnection::factory()->{$dbDriver}()->create([
                'endpoint_id' => $endpoint->id,
            ]);
        }
        $token = Token::factory()->create(['system' => true] + $request->toArray());

        $restClient = new Client(Credentials::makeInstance($endpoint, $token));
        $restClient->exec('app.option.set', ['options' => [
            'fingerprint' => $endpoint->fingerprint,
        ]]);
        if ($restErrors = $restClient->getErrors()) {
            return ['errors' => $restErrors];
        }

        return ['data' => 1];
    }

    protected function initEndpoint(Request $request): Endpoint
    {
        if ($endpoint = Endpoint::where('code', $request->input('member_id'))->first()) {
            return $endpoint;
        }

        return Endpoint::factory()->create([
            'code' => $request->input('member_id'),
            'host' => $request->input('DOMAIN'),
            'app_id' => $request->input('app_id') ?: '',
            'app_secret' => $request->input('app_secret') ?: '',
        ])->refresh();
    }
}
