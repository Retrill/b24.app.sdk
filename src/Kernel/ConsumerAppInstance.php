<?php

namespace Glu\B24AppBackground\Kernel;

class ConsumerAppInstance
{
    /*
     * one consumer (b24 portal) may have several app instances (i.e. several app sites - for example, techsupport domains)
     *
     * install - adds new instance (consumer db-record, new database)
     * uninstall - remove information about instance
     * archivate - dump database and consumer info to .tar.gz
     * gets consumer connection info
     * checks this consumer is registered?
     */

    public function __construct()
    {
    }
}
