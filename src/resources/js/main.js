// import { ref } from 'vue';
import { createApp } from 'vue';
import { createI18n } from 'vue-i18n';
import AppComponent from './App.vue';

import ru from './locale/ru.json'
import en from './locale/en.json'

console.log('ru: ', ru);

const i18n = createI18n({
    locale: 'ru',
    messages: {ru, en}
});

const app = createApp(AppComponent);

app.use(i18n);
app.mount('#app');

