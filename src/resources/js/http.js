export default {
    post: function(endpoint, data) {
        return this.request('post', endpoint, data);
    },
    request: (method, endpoint, data) => new Promise((resolve, reject) => {
        axios[method](endpoint, data)
        .then((response) => {
            console.log('response: ', response);
            try {
                let payload = {};
                let responseErrors = [];
                if (typeof response.data == 'object') {
                    payload = response.data.data;
                    responseErrors = ('errors' in response.data) ? response.data.errors : responseErrors;
                }else{
                    let responseJson = JSON.parse(response.data);
                    payload = responseJson.data;
                    responseErrors = ('errors' in responseJson) ? responseJson.errors : responseErrors;
                }

                if(responseErrors.length > 0){
                    reject(responseErrors, response.status);
                }else{
                    resolve(payload);
                }
            } catch (errorText) {
                let msg = "unexpected non-json ajax response: ";
                console.error(msg, response.data);
                console.log(errorText);
                reject([errorText], response.status);
            }
        })
        .catch(function (error) {
            if (error.response) {
                // Request made and server responded
                let msg = "Ajax response error! status code = " + error.response.status;
                console.error(msg + "; ", error.response.data);
                console.log("response headers: ", error.response.headers);
                reject([msg], error.response.status);
            } else if (error.request) {
                let msg = "The request was made but no response was received";
                console.error(msg + ": ", error.request);
                reject([msg], 0);
            } else {
                let msg = "Something happened in setting up the request that triggered an Error";
                console.error(msg + ": ", error.message);
                reject([msg], 0);
            }
        });
    })

}
