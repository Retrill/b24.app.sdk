<?php

use Illuminate\Support\Facades\Route;

$namespace = 'Glu\\B24AppBackground\\Controllers\\';

/* Route::match(
    ['get', 'post'],
    '/install/{step}',
    $namespace.'Installer'
)->name('install'); */

Route::match(['get', 'post'], '/install/fake', function(Request $request){
    return view('glu/b24.app.background::fakeinstall');
});

Route::post('/install/{step}', $namespace.'Installer')->name('install');


