<?php

namespace Glu\B24AppBackground\Models\Consumer;

use Glu\B24AppBackground\Db\Factories\Consumer\TokenFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Token extends ConsumerConnectionBased
{
    use HasFactory;

    // protected $fillable = [
    //     'token',
    // ];
    protected $guarded = [];
    protected $dates = ['created_at', 'updated_at', 'valid_through'];

    protected static function newFactory()
    {
        return TokenFactory::new();
    }

    // protected $dispatchesEvents = [
    //     'booting' => UserSaved::class,
    // ];

    /* public function __construct(array $attributes = [])
    {
        $this->setConnection(\app('consumer_connection'));

        return parent::{__FUNCTION__}($attributes);
    } */

    /* protected static function booting()
    {
        parent::booting();
        dump('model booting!');

        static::booting(function ($model) {
            dd('model boot');
        });
    } */
}
