<?php

namespace Glu\B24AppBackground\Models\Consumer;

use Illuminate\Database\Eloquent\Model;

class ConsumerConnectionBased extends Model
{
    protected $connection;

    public function __construct(array $attributes = [])
    {
        $this->connection = \env('DB_CONSUMER_CONNECTION_NAME', 'consumer_connection');
        return parent::{__FUNCTION__}($attributes);
    }
}
