<?php

namespace Glu\B24AppBackground\Models;

use Glu\B24AppBackground\Db\Factories\ConsumerConnectionFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ConsumerConnection extends Model
{
    use HasFactory;
    protected $guarded = [];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        // 'saved' => UserSaved::class,
        // 'deleted' => UserDeleted::class,
    ];

    protected static function newFactory()
    {
        return ConsumerConnectionFactory::new();
    }

    public function endpoint()
    {
        return $this->belongsTo(Endpoint::class, 'endpoint_id');
    }
}
