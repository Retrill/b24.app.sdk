<?php

namespace Glu\B24AppBackground\Models;

use Glu\B24AppBackground\Db\Factories\EndpointFactory;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Endpoint extends Model
{
    use HasFactory;

    protected $table = 'endpoints';
    protected $fillable = ['host', 'code', 'fingerprint'];

    public function connections()
    {
        return $this->hasMany(ConsumerConnection::class, 'endpoint_id');
    }

    protected static function newFactory()
    {
        return EndpointFactory::new();
    }
}
