<?php

namespace Glu\B24AppBackground;

use Glu\B24AppBackground\Console\Commands\Migrate;
use Glu\B24AppBackground\Console\Commands\MigrateFresh;
use Illuminate\Contracts\Events\Dispatcher;

class MigrationServiceProvider extends \Illuminate\Database\MigrationServiceProvider
{
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        parent::{__FUNCTION__}();

        $this->registerMigrateCommand();
    }

    /**
     * Register the command.
     *
     * @return void
     */
    protected function registerMigrateCommand()
    {
        $this->app->singleton('command.migrate', function ($app) {
            return new Migrate($app['migrator'], $app[Dispatcher::class]);
        });
        $this->app->singleton('command.migrate.fresh', function ($app) {
            return new MigrateFresh($app['migrator'], $app[Dispatcher::class]);
        });
    }
}
