<?php

namespace Glu\B24AppBackground\Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Tests\TestCase;

class HttpTest extends TestCase
{
    use DatabaseMigrations;

    protected $appurl;

    public function setUp(): void
    {
        parent::{__FUNCTION__}();
        $this->appurl = \env('APP_URL');
    }

    public function testAppIsAccessibleByGet()
    {
        $this->app['env'] = 'development';
        $response = $this->get($this->appurl);

        $response->assertStatus(200);
    }

    public function testAppIsLockedByPostWithoutCsrf()
    {
        $this->app['env'] = 'development';
        $response = $this->post($this->appurl, []);
        $response->assertStatus(419);
    }

    public function testAppIsAccessibleByPostWithCsrfAndOrigin()
    {
        $this->app['env'] = 'development';

        $fakeCsrf = md5(time());

        $response = $this->withHeaders([
            'Origin' => 'https://untrusted.ru',
            'X-CSRF-TOKEN' => $fakeCsrf,
            'Accept' => 'application/json',
            // 'Content-Type' => 'application/json',
        ])->withSession([
            '_token' => $fakeCsrf,
        ])->post($this->appurl, [
            'DOMAIN' => '',
        ]);

        $response->assertStatus(403); // 403 is better than 419 in this case
    }

    public function testAppIsAccessibleByPostWithOriginOnly()
    {
        $this->app['env'] = 'development';
        $domain = \preg_replace('/^https?:\/\//i', '', $this->appurl);

        $response = $this->post('/', [
            'DOMAIN' => $domain,
        ], [
            'HTTP_ORIGIN' => 'https://'.$domain,
            'Accept' => 'application/json',
        ]);

        $response->assertStatus(403); // 403 is better than 419 in this case
    }
}
