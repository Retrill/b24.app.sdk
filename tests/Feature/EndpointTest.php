<?php

namespace Glu\B24AppBackground\Tests\Feature;

use Glu\B24AppBackground\Models\Endpoint;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Str;
use Tests\TestCase;

class EndpointTest extends TestCase
{
    use DatabaseMigrations;

    protected $appurl;

    public function setUp(): void
    {
        parent::{__FUNCTION__}();
        $this->appurl = env('APP_URL');
    }

    public function testEndpointModelIsUnique()
    {
        $fakeEndpoint = [
            'code' => \md5(\time()),
            'host' => Str::randomDict(50, 'A-Z a-z \d').'.com',
            'fingerprint' => Str::randomDict(255, 'A-Z a-z \d'),
        ];
        $endpoint = Endpoint::factory()->create($fakeEndpoint);
        // Endpoint::create($fakeEndpoint);
        $this->expectException(QueryException::class);
        $replica = $endpoint->replicate();
        $replica->save();
    }
}
