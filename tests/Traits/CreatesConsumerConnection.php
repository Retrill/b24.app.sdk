<?php

namespace Glu\B24AppBackground\Tests\Traits;
use Illuminate\Support\Str;
use Glu\B24AppBackground\Models\{Endpoint, ConsumerConnection};
use Illuminate\Support\Facades\DB;

Trait CreatesConsumerConnection
{
    protected function createConsumerConnection()
    {
        $endpoint = Endpoint::factory()->create([
            'code' => Str::randomDict(32, 'a-z \d'),
            'host' => 'b24-'.Str::randomDict(6, 'a-z \d').'.bitrix24.ru',
        ]);

        $driver = \config('database.connections.'.\env('DB_CONNECTION'))['driver'];
        $consumerConnection = ConsumerConnection::factory()->{$driver}()->create([
            'endpoint_id' => $endpoint->id,
        ]);

        $this->app['consumer_connection_id'] = $consumerConnection->id;
    }
}
