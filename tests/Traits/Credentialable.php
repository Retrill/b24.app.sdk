<?php

namespace Glu\B24AppBackground\Tests\Traits;
use Illuminate\Support\Str;

Trait Credentialable
{
    protected function generateB24Credentials()
    {
        return [
            'AUTH_ID' => Str::randomDict(70, 'a-z \d'),
            'AUTH_EXPIRES' => '3600',
            'REFRESH_ID' => Str::randomDict(70, 'a-z \d'),
            'member_id' => Str::randomDict(32, 'a-z \d'),
            'status' => 'L',
            'PLACEMENT' => 'DEFAULT',
            'PLACEMENT_OPTIONS' => '{"IFRAME":"Y","IFRAME_TYPE":"SIDE_SLIDER"}',
            'DOMAIN' => 'b24-'.Str::randomDict(6, 'a-z \d').'.bitrix24.ru',
            'PROTOCOL' => '1',
            'LANG' => 'ru',
            'APP_SID' => Str::randomDict(32, 'a-z \d'),
        ];
    }
}
