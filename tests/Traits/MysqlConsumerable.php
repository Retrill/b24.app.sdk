<?php

namespace Glu\B24AppBackground\Tests\Traits;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

Trait MysqlConsumerable
{
    public function setUp(): void
    {
        parent::{__FUNCTION__}();

        $driver = \config('database.connections.'.\env('DB_CONNECTION'))['driver'];
        if ('sqlite' != $driver) {
            // Clean created consumer users && databases

            $consumerDbTpl = \strtr(\env('DB_CONSUMER_DB_TPL'), [
                '{endpoint_id}' => '%',
                '{connection_id}' => '%',
            ]);
            $this->beforeApplicationDestroyed(function () use ($consumerDbTpl) {
                /* $users = DB::table('mysql.user')->where('user', 'like', '"testapp_cons%"')->get();
                foreach ($users as $user) {
                    \dump($user->toArray());
                } */
                $consumerDatabases = DB::select(\sprintf('SHOW DATABASES LIKE "%s"', $consumerDbTpl));
                foreach ($consumerDatabases as $database) {
                    $dbName = \reset($database);
                    DB::statement(\sprintf('DROP DATABASE %s', $dbName));
                    if ((bool) \preg_match('/^(?<consumer>.*)(_db\d+)$/', $dbName, $matches)) {
                        DB::statement(\sprintf('DROP USER \'%s\'@\'%s\'', $matches['consumer'], \env('DB_CONSUMER_ALLOWED_HOST_FROM', '127.0.0.1')));
                    }
                }
            });
        }
    }
}
