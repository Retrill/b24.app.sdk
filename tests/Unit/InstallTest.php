<?php

namespace Glu\B24AppBackground\Tests\Unit;

use Glu\B24AppBackground\Models\{Endpoint, Consumer\Token};
use Glu\B24AppBackground\Tests\Traits\Credentialable;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class InstallTest extends TestCase
{
    use DatabaseMigrations, Credentialable;

    protected $appurl;

    public function setUp(): void
    {
        parent::{__FUNCTION__}();
        $this->appurl = env('APP_URL');
    }

    public function testInstallationCredentialsCannotBeReceivedViaGet()
    {
        $credentials = $this->sendB24Credentials('get');
        $this->assertFalse((Endpoint::where('code', $credentials['member_id']))->exists());
    }

    public function testInstallationCredentialsCanBeReceivedViaPost()
    {
        $credentials = $this->sendB24Credentials('post');
        $this->assertTrue((Endpoint::where('code', $credentials['member_id']))->exists());
    }

    protected function sendB24Credentials(string $httpMethod, ?array $credentials = null)
    {
        $credentials ??= $this->generateB24Credentials();
        $this->{$httpMethod}(\route('install', 1), $credentials);

        return $credentials;
    }

    public function testApplicationCannotBeInstalledTwice()
    {
        $credentials = $this->generateB24Credentials();
        //Double request:
        $this->sendB24Credentials('post', $credentials);
        $this->sendB24Credentials('post', $credentials);
        $this->assertEquals(1, (Endpoint::where('code', $credentials['member_id']))->count());
    }

    public function testInstallationProducesCorrectEndpoint(){
        $credentials = $this->generateB24Credentials();
        $this->sendB24Credentials('post', [
            'DOMAIN' => 'https://'.$credentials['DOMAIN']
        ] + $credentials);
        $endpoint = Endpoint::first();

        $this->assertFalse((bool) \preg_match('/http/i', $endpoint->host), 'Endpoint host should not starts with "http"');

        $this->assertTrue($endpoint->host == $credentials['DOMAIN'], 'Domain name mismatch');
        $this->assertTrue($endpoint->uri == '/rest/', 'URI mismatch');
        $this->assertTrue($endpoint->code == $credentials['member_id'], 'Member id mismatch');
        $this->assertEquals(1, $endpoint->authenticable, 'Default endpoint "authenticable" value should be "1"');
        $this->assertEmpty($endpoint->app_id, 'Default app id should be empty');
        $this->assertEmpty($endpoint->app_secret, 'Default app secret should be empty');
        $this->assertNotEmpty($endpoint->fingerprint, 'Fingerprint should be not empty');
        $this->assertLessThanOrEqual(5, time() - $endpoint->created_at->timestamp, 'Endpoind "created at" differs from php timestamp more than 5 seconds');
    }

    public function testInstallationProducesCorrectToken(){
        $credentials = $this->generateB24Credentials();
        $this->sendB24Credentials('post', $credentials);
        $token = Token::first();

        $consConnectionName = \env('DB_CONSUMER_CONNECTION_NAME', 'consumer_connection');
        $this->assertEquals(
            $token->auth_token,
            DB::connection($consConnectionName)->table('tokens')->find($token->id)->auth_token,
            'Token should be saved under the consumer connection (not under the basic connection)'
        );

        $this->assertGreaterThan(3000, $token->valid_through->timestamp - $token->created_at->timestamp, 'Just created token should be valid');
        $this->assertGreaterThan(time(), $token->valid_through->timestamp, '"Valid through" should be a moment in future');
        $this->assertEquals($token->auth_token, $credentials['AUTH_ID'], 'Saved auth token should be equal to the source value');
        $this->assertEquals($token->refresh_token, $credentials['REFRESH_ID'], 'Saved refresh token should be equal to the source value');
        $this->assertNotEquals($token->auth_token, $token->refresh_token, 'Auth token and refresh token should not be the same');
        $this->assertEquals($token->lang, 'ru', 'Default language should be "ru"');
        $this->assertEquals($token->system, 1, 'Installation token should be system');

        $regularToken = Token::factory()->create($this->generateB24Credentials())->refresh();
        $this->assertEquals($regularToken->system, 0, 'Regular token should not has a system value');
    }
}
