<?php

namespace Glu\B24AppBackground\Tests\Unit;

use Glu\B24AppBackground\Models\Consumer\Token;
use Glu\B24AppBackground\Models\ConsumerConnection;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;
use Glu\B24AppBackground\Tests\Traits\{Credentialable, CreatesConsumerConnection};

class ConsumerConnectionTest extends TestCase
{
    use DatabaseMigrations, Credentialable, CreatesConsumerConnection;

    protected $appurl;

    public function setUp(): void
    {
        parent::{__FUNCTION__}();
        $this->appurl = \env('APP_URL');
    }

    public function testConsumerConnectionMayBeCreatedAndMigrated()
    {
        $this->createConsumerConnection();
        $consConnectionName = \env('DB_CONSUMER_CONNECTION_NAME', 'consumer_connection');
        $count = DB::connection($consConnectionName)->table('migrations')->count();
        $this->assertGreaterThan(0, $count, 'Number of consumer migrations should be greater than 0');

        $token = Token::factory()->create($this->generateB24Credentials());
        $this->assertIsInt($token->id, 'Auth token should be saved');
        $this->assertEquals(
            $token->auth_token,
            DB::connection($consConnectionName)->table('tokens')->find($token->id)->auth_token,
            'Tokens should be saved under the consumer connection table'
        );
    }
}
